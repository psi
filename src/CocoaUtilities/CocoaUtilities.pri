INCLUDEPATH *= $$PWD/..
DEPENDPATH *= $$PWD $$PWD/..

HEADERS += \
	$$PWD/CocoaInitializer.h \
	$$PWD/cocoacommon.h

OBJECTIVE_SOURCES += \
	$$PWD/CocoaInitializer.mm \
	$$PWD/cocoacommon.mm
